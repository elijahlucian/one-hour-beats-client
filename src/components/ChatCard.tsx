import React from 'react'
import { Chat } from '../types/view'
import { Row, Col, Avatar, Tooltip } from 'antd'
import { useUserContext } from 'contexts/UserContext'
import { userIndex } from 'mock/users'

type Props = {
  chat: Chat
}

export const ChatCard = ({ chat }: Props) => {
  //get logged in user
  const { user } = useUserContext()
  const otherUser = userIndex[chat.userId]

  //If the chat id equals login user, reverse chat bubbles
  if (chat.userId === user.id) {
    return (
      <Row align="bottom" justify="end" gutter={[2, 16]}>
        <Col span={16}>
          <div
            style={{ borderRadius: 15, backgroundColor: '#cfdcfd', padding: 5 }}
          >
            {chat.message}
          </div>
        </Col>
        <Col span={1}>
          <Tooltip placement="right" title={user.name}>
            <Avatar
              style={{
                color: '#0041f5',
                backgroundColor: '#cfdcfd',
                float: 'right',
              }}
            >
              {user.name[0]}
            </Avatar>
          </Tooltip>
        </Col>
      </Row>
    )
  } else {
    return (
      <Row align="bottom" gutter={[2, 16]}>
        <Col span={1}>
          <Tooltip placement="left" title={chat.userId}>
            <Avatar
              style={{
                color: '#f56a00',
                backgroundColor: '#fde3cf',
              }}
            >
              {otherUser.name[0]}
            </Avatar>
          </Tooltip>
        </Col>
        <Col span={16}>
          <div
            style={{ borderRadius: 15, backgroundColor: '#fde3cf', padding: 5 }}
          >
            {chat.message}
          </div>
        </Col>
      </Row>
    )
  }
}
